#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"

struct fileMetadata arr[3];
conta = 0;

void llenar(){
	for (cont3 = 0; cont3 < 3; cont3++){
		arr[cont3].tamano = 0;
		memcpy(arr[cont3].name, "NoName", 6);
		for (cont4 = 0; cont4 < 64; cont4++){
			arr[cont3].indices[cont4] = 0;
		}
	}
}

struct fileMetadata searchMeta(int dh, char *file_name){
	char* buffer;
	struct fileMetadata metad;
	metad.tamano = -1;
	for (cont3 = 5; cont3 <= 26; cont3++){
		buffer = malloc(1024);
		dev_read_block(dh, buffer, cont3);
		memcpy(arr, buffer, 876);
		for (cont4 = 0; cont4 < 2; cont4++){
			if (strncmp(arr[cont4].name, file_name, strlen(file_name)) == 0){
				free(buffer);
				metad = arr[cont4];
				return metad;
			}
		}
	}
	free(buffer);
	return metad;
}

int format_device(char *dev_name){
	struct DHandle *nodo;
	nodo = search(llave);
	llenar();
	if (nodo->f != NULL){
		if (strcmp(nodo->device.name, dev_name) == 0){
			if (nodo->device.blockcount == 4097
				&& nodo->device.blocksize == 1024){
				char *buffer;
				buffer = malloc(1024);
				memset(buffer, 0, 1024);
				for (cont3 = 1; cont3 < 4097; cont3++){
					dev_write_block(nodo->key, buffer, cont3);
				}
				memset(buffer, 0, 1024);
				memcpy(buffer, arr, 876);
				for (cont3 = 5; cont3 < 26; cont3++){
					dev_write_block(nodo->key, buffer, cont3);
				}
				memset(buffer, 0, 1024);
				memset(buffer, 1, 26);
				dev_write_block(nodo->key, buffer, 1);
				nodo->device.format = 1;
				nodo->device.cantarch = 65;
				memset(buffer, 0, 1024);
				memcpy(buffer, &nodo->device, sizeof(struct Handle));
				dev_write_block(nodo->key, buffer, 0);
				free(buffer);
				return 1;
			}
		}
	}
	return 0;
}

int file_open(int dh, char *file_name){
	struct DHandle *nodo;
	struct fileMetadata metad;
	nodo = search(dh);
	if (nodo->f!=NULL && nodo->device.format==1){
		metad = searchMeta(dh, file_name);
		if (metad.tamano != -1 && conta < 10){
			files[conta].metadata = metad;
			files[conta].dh = dh;
			conta++;
			return 0;
		}
	}
	return -1;
}

int bloqueDisp(int dh){
	struct DHandle *nodo;
	nodo = search(dh);
	char *buffer;
	buffer = malloc(1024);
	if (nodo->f != NULL){
		for (cont3 = 27; cont3 < 4097; cont3++){
			dev_read_block(dh, buffer, cont3);
			if (buffer == 0){
				free(buffer);
				return cont3;
			}
		}
	}
	free(buffer);
	return -1;
}

int file_read(int fh, int pos, char *buffer, int size){
	struct DHandle *nodo;
	char *buff, *usobuff;
	buff = malloc(files[fh].metadata.tamano);
	usobuff = malloc(1024);
	if (pos + size>files[fh].metadata.tamano){
		size = files[fh].metadata.tamano - pos;
	}
	if (files[fh].dh != -1){
		nodo = search(files[fh].dh);
		if (nodo->f != NULL){
			for (cont3 = 0; cont3 < 64; cont3++){
				if (files[fh].metadata.indices[cont3] != 0){
					dev_read_block(files[fh].dh, usobuff, files[fh].metadata.indices[cont3]);
					for (cont4 = 0; cont4 < 1024; cont4++){
						if (usobuff[cont4] != 0)
							buff[cont4 + cont3 * 1024] = usobuff[cont4];
					}
				}
			}
		}
		for (cont3 = 0; cont3 < size; cont3++){
			buffer[cont3] = buff[cont3 + pos];
		}
		free(buff);
		free(usobuff);
		return 0;
	}
	free(buff);
	free(usobuff);
	return 1;
}

int file_write(int fh, int pos, char *buffer, int size){
	struct DHandle *nodo;
	char *buff, *usobuff;
	if (pos>files[fh].metadata.tamano)
		pos = files[fh].metadata.tamano;	
	usobuff = malloc(1024);
	if (pos + size>files[fh].metadata.tamano){
		buff = malloc(pos + size);
		if ((pos + size) / 1024 > files[fh].metadata.tamano / 1024)
			files[fh].metadata.indices[(pos + size) / 1024] = bloqueDisp(files[fh].dh);
	}
	else{
		buff = malloc(files[fh].metadata.tamano);
	}
	int pos1, val, bloque;
	if (files[fh].dh != -1){
		nodo = search(files[fh].dh);
		if (nodo->f != NULL){
			for (cont3 = 0; cont3 < 64; cont3++){
				if (files[fh].metadata.indices[cont3] != 0){
					dev_read_block(files[fh].dh, usobuff, files[fh].metadata.indices[cont3]);
					for (cont4 = 0; cont4 < 1024; cont4++){
						if (usobuff[cont4]!=0)
							buff[cont4+cont3*1024] = usobuff[cont4];
					}
				}
			}
			for (cont3 = 0; cont3 < size; cont3++){
				buff[cont3 + pos] = buffer[cont3];
			}
			for (cont3 = 0; cont3 < 64; cont3++){
				if (files[fh].metadata.indices[cont3] != 0){
					for (cont4 = 0; cont4 < 1024; cont4++){
						if (usobuff[cont4] != 0)
							usobuff[cont4] = buff[cont4 + (cont3 * 1024)];
					}
					dev_write_block(files[fh].dh, usobuff, files[fh].metadata.indices[cont3]);
				}
			}
			free(buff);
			return 1;
		}
	}
	free(buff);
	return 0;
}

int file_close(int fh){
	files[fh].dh = -1;
	conta = fh;
}

int file_create(int dh, char *file_name){
	struct fileMetadata meta;
	struct DHandle *nodo;
	nodo = search(dh);
	meta.tamano=0;
	memcpy(meta.name, file_name, strlen(file_name));
	if (nodo->device.cantarch == 65)
		nodo->device.cantarch = 0;
	int val=nodo->device.cantarch;
	if (nodo->f != NULL && nodo->device.format==1 && val<64){
		char *buffer;
		buffer = malloc(1024);
		memset(buffer, 0, 1024);
		dev_read_block(dh, buffer, 5 + val / 3);
		memcpy(arr, buffer, 876);
		arr[val%3] = meta;
		memcpy(buffer, arr, 876);
		dev_write_block(dh, buffer, 5 + val / 3);
		nodo->device.cantarch++;
		memset(buffer, 0, 1024);
		memcpy(buffer, &nodo->device, sizeof(struct Handle));
		dev_write_block(nodo->key, buffer, 0);
	}
}

int file_delete(int dh, char *file_name){
	struct DHandle *nodo;
	nodo = search(dh);
	char *buffer;
	if (nodo->f != NULL && nodo->device.format == 1){
		for (cont3 = 5; cont3 <= 26; cont3++){
			buffer = malloc(1024);
			memset(buffer, 0, 1024);
			dev_read_block(dh, buffer, cont3);
			memcpy(arr, buffer, 876);
			for (cont4 = 0; cont4 < 2; cont4++){
				if (strncmp(arr[cont4].name, file_name, strlen(file_name)) == 0){
					memset(arr[cont4].name, 0, 32);
					strncpy(arr[cont4].name, "NoName", 6);
					memset(buffer, 0, 1024);
					memcpy(buffer, arr, 876);
					dev_write_block(dh, buffer, cont3);
					memset(buffer, 0, 1024);
					nodo->device.cantarch = ((cont3 - 5) * 3) + cont4;
					memcpy(buffer, &nodo->device, 1024);
					dev_write_block(dh, buffer, 0);
					cont3 = 26;
					free(buffer);
					return 1;
				}
			}
		}
	}
}

int file_rename(int dh, char *file_name, char *file_namet){
	struct DHandle *nodo;
	nodo = search(dh);
	char* buffer;
	if (strncmp(file_name, file_namet, strlen(file_namet)) != 0 && nodo->device.format==1 && nodo->f !=NULL){
		for (cont3 = 5; cont3 <= 26; cont3++){
			buffer = malloc(1024);
			memset(buffer, 0, 1024);
			dev_read_block(dh, buffer, cont3);
			memcpy(arr, buffer, 876);
			for (cont4 = 0; cont4 < 2; cont4++){
				if (strncmp(arr[cont4].name, file_name, strlen(file_name)) == 0){
					free(buffer);
					memset(arr[cont4].name, 0, 32);
					strncpy(arr[cont4].name, file_namet, 32);
					memset(buffer, 0, 1024);
					memcpy(buffer, arr, 876);
					dev_write_block(dh, buffer, cont3);
					cont3 = 26;
					free(buffer);
					return 1;
				}
			}
		}
	}
	return 0;
}