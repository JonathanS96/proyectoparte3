#include "device.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cont=0;
int menordisp=0;


int menorDisp(){
}

int dev_format(char *name, int bsize, int bcount){
    FILE *f;
	char *buffer;
    if(bsize>=52){
		buffer = malloc(bsize);
        memset(buffer, 0, bsize);
        f=fopen(name, "w");
        if(f!=NULL){
            if(bsize>=sizeof(struct Handle)){
                for(cont=0; cont<bcount+1; cont++){
                    fwrite(buffer, 1, bsize, f);
                }
                struct Handle *handle=(struct Handle*)buffer;
                memcpy(handle->signature, "JHSV", 4);
                memcpy(handle->name, name, strlen(name));
                handle->blocksize=bsize;
                handle->blockcount=bcount;
				handle->format = 0;
				handle->cantarch = 0;
                fseek(f, 0, SEEK_SET);
                fwrite(buffer, 1, bsize, f);
                fclose(f);
				free(buffer);
                return 0;
            }
        }
		free(buffer);
    }
    return -1;
}

int dev_open(char *name){
	struct DHandle *nodo;
	nodo = malloc(sizeof(struct DHandle));
	nodo->key = llave;
    nodo->f=fopen(name, "r+");
    if(nodo->f!=NULL){
        fread(&nodo->device, 1, 52, nodo->f);
        //printf("hizo esto");
		if (memcmp(nodo->device.signature, "JHSV", 4) == 0){
            //printf("%s, %s, %d, %d\n", deviceHandle[menordisp].signature, deviceHandle[menordisp].name, deviceHandle[menordisp].blocksize, deviceHandle[menordisp].blockcount);
			add(nodo);
			return 0;
        }
    }
    return -1;
}


int dev_read_block(int  dh, char *buffer, int bindex){
	struct DHandle *nodo;
	nodo = search(dh);
    memset(buffer, 0, sizeof(buffer));
    if(nodo->f!=NULL && bindex<=nodo->device.blockcount){
		fseek(nodo->f, bindex*nodo->device.blocksize, SEEK_SET);
        //printf("%d\n",deviceHandle[dh].blocksize);
		fread(buffer, 1, nodo->device.blocksize, nodo->f);
        //printf("%s\n", buffer);
        return 0;
    }
    return -1;
}

int dev_write_block(int dh, char *buffer, int bindex){
	struct DHandle *nodo;
	nodo = search(dh);
	if (nodo->f != NULL && bindex <= nodo->device.blockcount){
		fseek(nodo->f, bindex*nodo->device.blocksize, SEEK_SET);
		fwrite(buffer, 1, nodo->device.blocksize, nodo->f);
        return 0;
    }
    return -1;
}

int dev_close(int dh){
	struct DHandle *nodo;
	nodo = search(dh);
	if (nodo->f != NULL){
		fclose(nodo->f);
        return 0;
    }
    return -1;
}
