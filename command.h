#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef COMMAND_H_
#define COMMAND_H_

int correr(int argc, char *argv[]);
void processLine(char *line);

#endif
