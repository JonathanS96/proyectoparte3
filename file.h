#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

#ifndef FILE_H_
#define FILE_H_

struct fileMetadata{
	char name[32];
	int tamano, indices[64];
};

struct FileHandle{
	struct fileMetadata metadata;
	char dh;
};


struct FileHandle files[10];

int cont3;
int cont4;
int conta;

void llenar();

int format_device(char *dev_name);

int file_open(int dh, char *file_name);

int file_read(int fh, int pos, char *buffer, int size);

int file_write(int fh, int pos, char *buffer, int size);

int file_close(int fh);

int file_create(int dh, char *file_name);

int file_delete(int dh, char *file_name);

int file_rename(int dh, char *file_name, char *file_namet);

#endif
