#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"

#ifndef HASH_H_
#define HASH_H_

char llave;

struct list array[10000];

struct Handle{
	char signature[4], name[32];
	int blocksize, blockcount;
	int format, cantarch;
};

struct DHandle{
	struct Handle device;
	FILE *f;
	char key;
	struct list_elem elem;
};

int hashFunction(char key);

int add(struct DHandle *nodo);

struct DHandle* search(char key);

#endif