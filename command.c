#include <stdio.h>
#include <stdlib.h>		/* for free() */
#include <string.h>
#include <editline/readline.h>
#include "device.h"
#include "file.h"

char* param[5];
char buffer[1500];
int cont1=0;
int cont2=2;
struct list_elem *a;

void processLine(char *line)
{
    char *p;
    cont1=0;

    if (strcmp(line, "clear history") == 0) {
        clear_history();
    } else {
        add_history(line);
    }

    p = strtok(line, " ");
    //printf("Command is '%s'\n", p);
    param[cont1]=p;
    cont1++; cont2++;

    p = strtok(NULL, " ");
    while (p != NULL) {
        //printf("Argument: '%s'\n", p);
        param[cont1]=p;
        cont1++; cont2++;
        p = strtok(NULL, " ");
    }

}

int correr(int argc, char *argv[])
{
    const char *prompt = "$ ";
    char *line;



    if (argc > 1) {
        ++argv, --argc; /* The first argument is the program name */
    }

    while (1) {
        line = readline(prompt);


		if (line == NULL) {
            break;
        }

        processLine(line);

        //CREATE DEVICE
        if(strcmp(param[0], "create")==0 && strcmp(param[1], "device")==0){
            if(param[2]!=NULL && param[3]!=NULL && param[4]!=NULL){
               int val= dev_format(param[2], atoi(param[3]), atoi(param[4]));
			   if (val == 0){
				   printf("Se creo el Device\n");
			   }
			   else{
				   printf("No se pudo crear el Device\n");
			   }
            }
        }

		//CREATE FILE
		if (strcmp(param[0], "create") == 0 && strcmp(param[1], "file") == 0){
			if (param[2] != NULL && param[3] != NULL){
				file_create(*param[2], param[3]);
			}
		}

        //OPEN DEVICE
        if(strcmp(param[0], "open")==0 && strcmp(param[1], "device")==0 && strcmp(param[3], "as")==0){
            if(param[2]!=NULL && param[4]!=NULL){
				llave = *param[4];
				int val = dev_open(param[2]);
				if (val == 0){
					printf("Se abrio el Device\n");
				}
				else{
					printf("No se pudo abrir el Device\n");
				}
            }
        }

		//OPEN FILE
		if (strcmp(param[0], "open") == 0 && strcmp(param[1], "file") == 0){
			if (param[2] != NULL && param[3] != NULL){
				file_open(*param[2], param[3]);
			}
		}

		//FORMAT DEVICE
		if (strcmp(param[0], "format") == 0 && strcmp(param[1], "device") == 0){
			if (param[2] != NULL && param[3] != NULL){
				llave = *param[3];
				format_device(param[2]);
			}
		}

        //SHOW OPEN DEVICES
        if(strcmp(param[0], "show")==0 && strcmp(param[1], "open")==0 && strcmp(param[2], "devices")==0){
			struct DHandle *nodo;
            for(cont1=0; cont1<10000; cont1++){
				if (!list_empty(&array[cont1])){
					for (a = list_rbegin(&array[cont1]); a != list_rend(&array[cont1]); a = list_prev(a)) {
						nodo = list_entry(a, struct DHandle, elem);
						printf("%s, %c\n", nodo->device.name, nodo->key);
					}
                }
            }
        }

		//SHOW OPEN FILES
		if (strcmp(param[0], "show") == 0 && strcmp(param[1], "open") == 0 && strcmp(param[2], "files") == 0){
			for (cont1 = 0; cont1<10; cont1++){
				if (files[cont1].dh != -1){
					printf("%s, %c\n", files[cont1].metadata.name, files[cont1].dh);
				}
			}
		}

        //SHOW METADATA
        if(strcmp(param[0], "show")==0 && strcmp(param[1], "metadata")==0 && strcmp(param[2], "from")==0){
            if(param[3]!=NULL){
				struct DHandle *nodo;
				nodo = search(*param[3]);
                if(nodo->f != NULL){
					printf("%s, %d, %d\n", nodo->device.name, nodo->device.blocksize, nodo->device.blockcount);
                }
            }
        }

        //READ BLOCK
        if(strcmp(param[0], "read")==0 && strcmp(param[1], "block")==0){
            if(param[2]!=NULL && param[3]!=NULL){
                dev_read_block(*param[2], buffer, atoi(param[3]));

				if (cont2 == 4){
					for (cont1 = 0; cont1 < 32; cont1++){
						printf("%x", buffer[cont1]);
						}
						printf("\n");
				}
				else{
					int num = atoi(param[4]);
					if (num < sizeof(buffer)){
						for (cont1 = 0; cont1 < num; cont1++){
							printf("%x", buffer[cont1]);
						}
						printf("\n");
					}
					else{
						for (cont1 = 0; cont1 < sizeof(buffer); cont1++){
							printf("%x", buffer[cont1]);
						}
						printf("\n");
					}

				}
            }
        }

		//RENAME FILE
		if (strcmp(param[0], "rename") == 0 && strcmp(param[1], "file") == 0){
			if (param[2] != NULL && param[3] != NULL && param[4] != NULL){
				file_rename(*param[2], param[3], param[4]);
			}
		}

		//WRITE FILE
		if (strcmp(param[0], "writefile") == 0){
		if (param[1] != NULL && param[2] != NULL && param[3] != NULL && param[4] != NULL){
				file_write(atoi(param[1]), atoi(param[2]), param[3], atoi(param[4]));
			}
		}

		//DELETE FILE
		if (strcmp(param[0], "delete") == 0 && strcmp(param[1], "file") == 0){
			if (param[2] != NULL && param[3] != NULL){
				file_delete(*param[2], param[3]);
			}
		}

        //CLOSE DEVICE
        if(strcmp(param[0], "close")==0 && strcmp(param[1], "device")==0){
            if(param[2]!=NULL){
				int val=dev_close(*param[2]);
				if (val == 0){
					printf("Se cerro el Device\n");
				}
				else{
					printf("No se pudo cerrar el Device\n");
				}
            }
        }

        if (*line == 0) {
            free(line);
	    continue;
        }

        if (strcmp(line, "exit") == 0 ||
                strcmp(line, "quit") == 0) {
            break;
        }

        if(strcmp(line, "prueba")==0){
            printf("Prueba");
        }
        cont2=0;
        free(line);
    }

    printf("\nExiting ...\n");

    return 0;
}
