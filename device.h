#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hash.h"

#ifndef DEVICE_H_
#define DEVICE_H_

int dev_format(char *name, int bsize, int bcount);

int dev_open(char *name);

int dev_read_block(int dh, char *buff, int bindex);

int dev_write_block(int dh, char *buff, int bindex);

int dev_close(int dh);

#endif
