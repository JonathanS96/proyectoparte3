#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct list_elem *e;

int hashFunction(char key){
	key = ~key + (key << 15);
	key = key ^ (key >> 12);
	key = key + (key << 2);
	key = key ^ (key >> 4);
	key = key * 2057;
	key = key ^ (key >> 16);
	return key % 10000;
}

struct DHandle* search(char key){
	struct DHandle *nodo;
	nodo = malloc(sizeof(struct DHandle));
	if (list_empty(&array[hashFunction(key)])){
		return nodo;
	}
	else{
		for (e = list_rbegin(&array[hashFunction(key)]); e != list_rend(&array[hashFunction(key)]); e = list_prev(e)) {
			nodo = list_entry(e, struct DHandle, elem);
			{
				if (nodo->key == key){
					return nodo;
				}
			}
		}
		nodo->f = NULL;
		return nodo;
	}
}

int add(struct DHandle *nodo){
	if (search(nodo->key)->f == NULL){
		return 0;
	}
	else{
		list_push_back(&array[hashFunction(nodo->key)], &nodo->elem);
		return 1;
	}
}